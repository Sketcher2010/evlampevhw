/**
 * Created by Виктор on 23.03.2015.
 */
import java.util.*;

class Node<T> {
    T elem;
    Node<T> left;
    Node<T> right;

    Integer sumR(Node<Integer> root) {
        if(root == null)
            return 0;
        else
            return root.elem+sumR(root.left)+sumR(root.right);
    }

    Integer sumSt(Node<T> root) {
        Stack<Node<T>> s = null; // Queue -- для того, чтобы по уровням идти

        Integer sum = 0;
        s.push(root);

        while(!s.isEmpty()) {
            Node<Integer> p = (Node<Integer>) s.pop();
            if(p != null) {
                sum += p.elem;
                s.push((Node<T>) p.left);
                s.push((Node<T>) p.right);
            }
        }
        return sum;
    }

    public Node<Integer> createTree(Scanner sc) {
        Integer x = sc.nextInt();
        if(x == 0)
            return null;
        else
        {
            Node<Integer> p = new Node<Integer>();
            p.elem = x;
            p.left = createTree(sc);
            p.right = createTree(sc);
            return p;
        }
    }

    Node<opNum> inputExpr(Scanner sc) {
        int x = sc.nextInt();
        Node<opNum> root = new Node<opNum>();
        if(x == 0) {
//            System.out.print("Ваше число: ");
            root.elem = new opNum();
            root.elem.isOperator = false;
            root.elem.number = sc.nextInt();
            root.left = null;
            root.right = null;
        } else {
//            System.out.print("Ваш оператор: ");
            root.elem = new opNum();
            root.elem.isOperator = true;
            root.elem.operator = sc.next();
            root.left = inputExpr(sc);
            root.right = inputExpr(sc);
        }
        return root;
    }

    public boolean find(Node<Integer> root, Integer x) {
        if(root == null)
            return false;
        if(root.elem == x)
            return true;
        else
        {
            if(root.elem > x)
                return find(root.left, x);
            else
                return find(root.right, x);
        }
    }

    public Node<Integer> add(Node<Integer> root, Integer x) {
        if(root == null) {
            root.elem = x;
            root.left = null;
            root.right = null;
        }
        else {
            if(root.elem > x)
                root.left = add(root.left, x);
            else
                root.right = add(root.right, x);
        }
        return root;
    }

    public Node<Integer> remove(Node<Integer> root, Integer x) {
        if(root == null) {
            return root;
        }
        if(root.elem == x)
        {
            root.elem = root.right.elem;
            Node<Integer> lRoot = root.left;
            while(root.left != null) {
                root = root.left;
            }
            root = lRoot;
        }
        else
        {
            if(root.elem > x)
                remove(root.left, x);
            else
                remove(root.right, x);
        }
        return root;
    }

    public void show(Node<T> root, int level) {
        String s = "";
        if(root != null) {
            for(int i = 0; i<level; i++)
                s += "--";
            System.out.println(s+root.elem);
            if(root.left !=null)
                show(root.left, level+1);
            if(root.right != null)
                show(root.right, level+1);
        }
    }
    
    public int countEven(Node<Integer> root) {
        int count = 0;
        Stack<Node<Integer>> ss = null; // Queue -- для того, чтобы по уровням идти

        ss.push(root);

        while(!ss.isEmpty()) {
            Node<Integer> p = (Node<Integer>) ss.pop();
            if(p != null) {
                if(p.elem % 2 == 0)
                    count++;
                ss.push((Node<Integer>) p.left);
                ss.push((Node<Integer>) p.right);
            }
        }
        return count;
    }

    int eval(Node<opNum> expr) {
        if(expr == null)
            return 0;
        if(!expr.elem.isOperator)
            return expr.elem.number;
        int l = eval(expr.left);
        int r = eval(expr.right);
//        System.out.println("DEBUG: "+expr.elem.operator+" "+l+" "+r);
        if(expr.elem.operator.equals("+"))
            return l+r;
        else if(expr.elem.operator.equals("-"))
            return l-r;
        else
            return l*r;
    }
}